import java.io.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Browser"})
public class Browser extends HttpServlet {
    
    private static final String footerTemplateFile = "templateFooter.html";
    private static final String navigationRulesFile = "navigationRules.txt";
    
    private String template = null;
    
    private HashMap<String, HashMap<String, String>> navigationRules = new HashMap<String, HashMap<String, String>>();
    private String indexPage = null;
    // Integer is unused, but using a map makes filling it easier
    private HashMap<String, Integer> validPages = new HashMap<String, Integer>();
    
    public void init() throws ServletException {
        super.init();
        template = readFooterTemplate();
        parseNavigationRules();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String page = request.getParameter("page");
        if (page == null)
        {
            page = indexPage;
        }
        
        response.setContentType("text/html");
        PrintWriter r = response.getWriter();
        // Validate page - if we have a rule for it, it's valid
        // else display a page that redirects to the index page
        if (!validPages.containsKey(page))
        {
            r.print("<html><h3>Page not found, redirecting you to the index page</h3></html>");
            response.sendRedirect(response.encodeRedirectURL("Browser?page=" + indexPage));
        }
        else
        {
            renderPage(page, r);
        }
    }

    @Override
    public String getServletInfo() {
        return "Lab1 Browser";
    }
    
    private String readFooterTemplate()
    {
       String template = new String("");
       try {
            ServletContext context = getServletConfig().getServletContext();
            String fullPath = context.getRealPath("/WEB-INF/" + footerTemplateFile);
            FileReader f = new FileReader(fullPath);
            BufferedReader b = new BufferedReader(f);
            String tmp = null;
            while ((tmp = b.readLine()) != null) {
                template += tmp;
            }
        }
        catch (IOException e) { System.out.println(e.getMessage()); }
        
        return template;       
    }
    
    private String parseNavigationRules()
    {
       String l = null;
       HashMap <String, String> tmpMap = null;
       try {
            ServletContext context = getServletConfig().getServletContext();
            String fullPath = context.getRealPath("/WEB-INF/" + navigationRulesFile);
            FileReader f = new FileReader(fullPath);
            BufferedReader b = new BufferedReader(f);
            while ((l = b.readLine()) != null)   {
                String[] t = l.trim().split("\\s*\\,\\s*");
                if (t.length < 3) {
                    indexPage = l;
                    continue;
                }
               
                tmpMap = navigationRules.get(t[0]);
                if (tmpMap == null)
                {
                    tmpMap = new HashMap<String, String>();
                    tmpMap.put(t[1], t[2]);
                    navigationRules.put(t[0], tmpMap);
                }
                else
                {
                    tmpMap.put(t[1], t[2]);
                }
                // Add to list of valid pages, both the page the rule is for
                // and the page the rules point to
                validPages.put(t[0], new Integer(1));
                validPages.put(t[2], new Integer(1));
            }
        }
        catch (Exception e) { System.out.println(e.getMessage()); }
        
       return l;       
    }
    
    private String renderPage(String page, PrintWriter out)
    {
       String l = null;
       try {
            ServletContext context = getServletConfig().getServletContext();
            String fullPath = context.getRealPath("/WEB-INF/" + page);
            FileReader f = new FileReader(fullPath);
            BufferedReader b = new BufferedReader(f);
            while ((l = b.readLine()) != null)   {
                if (l.contains("</body>"))
                    out.println(formatTemplateForPage(page));
                out.println(l);
            }
            //out.println("Navigation Rules:" + navigationRules);
        }
        catch (FileNotFoundException e) { out.println(e.getMessage()); }
        catch (IOException ignored) { }
        
       return l;       
    }
    
    private String formatTemplateForPage(String page)
    {
        String rendered = "";
        HashMap <String, String> pageActions = navigationRules.get(page);
        if ( pageActions != null) {
            String links = "";
            for (String key: pageActions.keySet()) {
                links += "<a href='/browser/Browser?page=" + pageActions.get(key) +"'>" + key + "</a><br>";
            }
            rendered = MessageFormat.format(template, links);
        }
        else
        {
            rendered = MessageFormat.format(template, "<a href='/browser/Browser?page=" + indexPage +"'>Index Page</a><br>");
        }

        return rendered;
    }
}