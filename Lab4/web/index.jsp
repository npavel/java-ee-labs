
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/style.css">
        <title>XML Based Query</title>
    </head>
    <body>
        <%@taglib tagdir="/WEB-INF/tags/" prefix="h" %>
        <div style="font-size: 12px; float: right">
            <a href="?locale=en_US">US</a> |
            <a href="?locale=en_GB">UK</a> |
            <a href="?locale=ro_RO">RO</a> |
            <a href="?locale=fr_FR">FR</a> |
            <a href="?locale=de_DE">DE</a> |
            <a href="?locale=de_CH">CH</a>
        </div>
        <div style="font-family: Arial">
            <h:dataView file="products.xml"/>
            <br/>
            <h:dataView file="purchase.xml"/>
        </div>
    </body>
</html>
