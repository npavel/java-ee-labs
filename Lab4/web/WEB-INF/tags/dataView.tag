<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@tag description="visualise sql queries" pageEncoding="UTF-8"%>

<%@attribute name="file"%>

<c:set var="locale" value="us_EN" scope="session"/>
<c:if test="${param['locale'] !=null}">
    <c:set var="locale" value= "${param['locale']}"/>        
    <fmt:setLocale value="${locale}" scope="session" />
</c:if>
<fmt:setBundle basename="message" var="msg"/>


<c:import var="dataViewXML" url="/${file}"/>
<x:parse doc="${dataViewXML}" var="xmlRoot" scope="application" />
<x:set var="query" select="string($xmlRoot/data/view/query)" />

<c:if test="${param['debug'] !=null}">
    File: ${file}<br>
    Query: ${query}<br>
    Locale: ${locale}<br>
    
    <x:forEach select="$xmlRoot/data/view/column" var="column" varStatus="index">
        <x:set var="label" select="string($column/@label)"/>
        <x:set var="width" select="string($column/@width)"/>
        <x:set var="align" select="string($column/@align)"/>
        ${label} - ${width} - ${align}<br>
    </x:forEach>    
</c:if>

<sql:setDataSource var="db" url="jdbc:derby://localhost:1527/sample" driver="org.apache.derby.jdbc.ClientDriver" user="app" password="app"/>    
<sql:query dataSource="${db}" sql="${query}" var="result" />

<br>
<table>
    <tr>
        <th>#</th>
        <c:forEach var="columnName" items="${result.columnNames}" varStatus="index">
            <c:set var="pos" value="${index.count}" />
            <x:set var="label" select="string($xmlRoot/data/view/column[position()=$pos]/@label)"/>
            <x:set var="width" select="string($xmlRoot/data/view/column[position()=$pos]/@width)"/>
            <x:set var="align" select="string($xmlRoot/data/view/column[position()=$pos]/@align)"/>            
            <th width="${width}" align="${align}">
                <!-- <c:out value="${columnName} renamed to ${label}"/> -->
                <fmt:message key="${label}" bundle="${msg}" />
            </th>
        </c:forEach>
    </tr>
    
    <c:forEach var="row" items="${result.rowsByIndex}" varStatus="index">
    <tr>
        <c:set var="pos" value="${index.count}" />
        <td>${pos}</td>
        <c:forEach var="col" items="${row}" varStatus="innerIndex">
            <c:set var="ipos" value="${innerIndex.count}" />
            <x:set var="type" select="string($xmlRoot/data/view/column[position()=$ipos]/@type)"/>

            <c:choose>
                <c:when test="${type eq 'currency'}">
                    <td><fmt:formatNumber value="${col}" type="currency"/></td>
                </c:when>
                <c:when test="${type eq 'number'}">
                    <td><fmt:formatNumber value="${col}" type="number"/></td>
                </c:when>
                <c:when test="${type eq 'date'}">
                    <td><fmt:formatDate value="${col}"/></td>
                </c:when>    
                <c:otherwise>
                    <td>${col}</td>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </tr>
    </c:forEach>
</table>