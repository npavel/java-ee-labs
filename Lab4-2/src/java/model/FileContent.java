package model;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;


public class FileContent {
    public String title;
    public String name;
    public String about;
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String t) {
        title = t;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String n) {
        name = n;
    }
    
    public String getAbout() {
        return about;
    }
    
    public void setAbout(String a) {
        about = a;
    }
    
    public void populateFromFile(String watchDir, String fileName) {
    try {
            FileReader f = new FileReader(new File(watchDir, fileName));
            BufferedReader b = new BufferedReader(f);
            setTitle(b.readLine());
            setName(b.readLine());
            setAbout(b.readLine());
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
