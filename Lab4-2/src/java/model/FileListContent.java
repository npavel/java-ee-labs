
package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileListContent {
    public ArrayList<String> list;

    public FileListContent() {
        this.list = new ArrayList<>();
    }

    public void setList(ArrayList<String> l) {
        list = l;
    } 
    
    public ArrayList<String> getList() {
        return list;
    }
    
    public void populateFromFile(String watchDir, String fileName) throws FileNotFoundException {
        Scanner s = new Scanner(new File(watchDir, fileName));
        
        while (s.hasNext()) {
            list.add(s.next());
        }
        
        s.close();
    }
}
