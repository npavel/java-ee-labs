import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.FileContent;
import model.FileListContent;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns={"/"}, asyncSupported=true)
public class AsyncServlet extends HttpServlet {
    private String watchDir;
    
    @Override
    public void init() throws ServletException {
        super.init();
        watchDir = getServletConfig().getInitParameter("watchDir");
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        final AsyncContext acontext = request.startAsync();
        acontext.start(new Runnable() {
            @Override
            public void run() {
                String fileName = null;
                String createdFile = null;
                boolean fileFound = false;
                FileListContent fileContent = new FileListContent();

                String dateParam = acontext.getRequest().getParameter("date");
                ServletResponse response = acontext.getResponse();
                
                //if (isValidDate(dateParam))
                //    fileName = "input_" + dateParam;

                fileName = "test";

                File f = new File(watchDir, fileName);
                if (f.exists())
                    fileFound = true;

                try {
                    WatchService watcher = FileSystems.getDefault().newWatchService();
                    Path watchPath = Paths.get(watchDir);
                    watchPath.register(watcher, ENTRY_CREATE);
 
                    while (!fileFound && fileName != null) {
                        WatchKey key = watcher.take();
                        for (WatchEvent<?> event : key.pollEvents()) {
                            WatchEvent.Kind<?> kind = event.kind();

                            if (kind == ENTRY_CREATE) {
                                createdFile = ((Path)event.context()).getFileName().toString();
                                System.out.println("New file created dir: " + createdFile);
                                if (createdFile.equals(fileName))
                                    fileFound = true;
                            }

                        }
                        key.reset();
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                
                try {
                    fileContent.populateFromFile(watchDir, fileName);
                    Map<String, Object> root = new HashMap<String, Object>();
                    root.put("fileEntries", fileContent.getList());
                    StringWriter content = applyTemplate(root);
                    
                    PrintWriter out = response.getWriter();
                    out.write(content.toString());
                    /*
                    out.printf("WatchDir: " + watchDir +  " Created file: " + createdFile +
                            "\nData: User: " + fileContent.getName() + " About: " + fileContent.getAbout() +
                            "\nThread %s completed the task", Thread.currentThread().getName());
                    */
                    out.flush();
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                } finally {
                    acontext.complete();
                }                
            }
        });
    }
    private static StringWriter applyTemplate(Map<String, Object> root) throws IOException, TemplateException {
        
        StringWriter out = new StringWriter();
        
        Configuration cfg;
        cfg = new Configuration();
        cfg.setServletContextForTemplateLoading( R.context.get_servletContext(), "/templates" );
        Template temp = cfg.getTemplate( "filelist.ftl" );
        temp.process( root, out );
        
        return out;
    }
    
    private static boolean isValidDate(String dateString) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy.MM.dd-hh.mm").parse(dateString);
        } catch (ParseException ex) {
            System.out.println("Invalid date format in " + dateString);
        }
        return date != null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Lab 4-2 Async Servlet Directory Watcher";
    }// </editor-fold>

}
