package spastandalone;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
    Algorithm for the full student-project allocation problem, as described at:
    http://eprints.gla.ac.uk/3439/
    The student oriented version is implemented.
*/

public class SPAStandalone
{
    public static int stud_count = 5;
    public static int prof_count = 3;
    public static int proj_count = 4;
    
    public static int[] prof_cap =
    {
        4, 3, 2
    };
    
    public static int[] proj_cap =
    {
   
        2, 2, 2, 2
    };
    
    public static int[][] stud_proj =
    {
        {0, 1},
        {1, 2},
        {3, 2, 1},
        {3, 0},
        {3, 2, 1, 0}
    };
    public static int[][] prof_proj =
    {
        {0, 1},
        {2},
        {3}
    };
    public static int[][] prof_stud =
    {
        {0},
        {1, 2, 3, 4},
        {3, 2, 1, 4}
    };
    
    public static class SPAData
    {
        public int stud_count;
        public int prof_count;
        public List<Integer> prof_cap;
        public List<Integer> proj_cap;
        public int proj_count;
        public Map<Integer, List<Integer>> stud_proj;
        public Map<Integer, List<Integer>> prof_stud;
        public Map<Integer, List<Integer>> prof_proj;
        public Map<Integer, Map<Integer, List<Integer>>> prof_proj_projected;
        public Map<Integer, List<Integer>> a_proj_stud;
        public Map<Integer, List<Integer>> a_prof_stud;
    };
    
    // Yeah, Java resists having a Pair or Tuple class...
    public static class Pair
    {
        Integer f = new Integer(-1);
        Integer s = new Integer(-1);
        public Pair(int a, int b)
        {
            f = new Integer(a);
            s = new Integer(b);
        }
    };
    
    SPAData d = null;
    
    private void init()
    {
    }
    
    private void buildProjectedLecturerPrefs()
    {
        System.out.println("prof - proj - projected students");
        for (int prof=0; prof<prof_count; ++prof)
        {
            Map<Integer, List<Integer>> proj_projected = new HashMap<Integer, List<Integer>>();
            for (int proj=0; proj<proj_count; ++proj)
            {
                List<Integer> proj_projected_students = new LinkedList<Integer>();
                // Iterate through prof's preferred students
                for (Integer stud: d.prof_stud.get(prof))
                {
                    // Find if the student actually wants the project
                    boolean wantsit = false;
                    List<Integer> student_prefs = d.stud_proj.get(stud);
                    if (student_prefs != null)
                    {
                        for (Integer proj_it: student_prefs)
                            if (proj_it.equals(proj))
                                wantsit = true;
                        if (wantsit)
                            proj_projected_students.add(stud);
                    }
                }
                System.out.print("plp: " + prof + " - " + proj + " - ");
                for (Integer i: proj_projected_students)
                    System.out.print(" " + i);
                System.out.println();
                proj_projected.put(proj, proj_projected_students);
            }
            d.prof_proj_projected.put(prof, proj_projected);
        }
    }
    
    private List<Integer> listFromCount(int count)
    {
        List<Integer> res = new LinkedList<Integer>();
        for(int i=0; i<count; ++i)
            res.add(i);
        return res;
    }
    
    private int findUnassignedStudentWithPrefs()
    {
        List<Integer> unassigned_studs = listFromCount(d.stud_count);
        for (Map.Entry<Integer, List<Integer>> e: d.a_proj_stud.entrySet())
        {
            for (Integer assigned_stud: e.getValue())
            {
                Iterator<Integer> it = unassigned_studs.iterator();
                while (it.hasNext())
                {
                    Integer ii = it.next();
                    if (assigned_stud.equals(ii))
                        it.remove();
                }
            }
        }
        if (unassigned_studs.size() == 0)
            return -1;
        int free_stud = -1;
        for (int stud: unassigned_studs)
        {
            if (d.stud_proj.size() > 0)
            {
                free_stud = stud;
                break;
            }
        }
        return free_stud;
    };
    
    private boolean studHasProj(int stud)
    {
        //TODO check for null?
        return (d.stud_proj.get(stud).size() > 0);
    };
    
    private int profForProj(int proj)
    {
        for(Map.Entry<Integer, List<Integer>> e: d.prof_proj.entrySet())
        {
            if (e.getValue().contains(proj))
            {
                return e.getKey();
            }
        }
        return -1;
    };
    
    // According to the projected preferences list
    private int worstStudForProj(int proj)
    {
        int worst = -1;
        int prof = profForProj(proj);
        List<Integer> studs = d.prof_proj_projected.get(prof).get(proj);
        List<Integer> studs_reversed = new LinkedList<>();
        for (Integer i: studs)
            studs_reversed.add(0, i);
        // From worst to best
        for (Integer i: studs_reversed)
        {
            // See if this guy is indeed assigned to this project
            List<Integer> ass = d.a_proj_stud.get(proj);
            for (Integer j: ass)
            {
                if (i.equals(j))
                {
                    worst = i;
                    break;
                }
            }
        }
        return worst;
    };
    
    // According to the professor's preference list
    private int worstStudForProf(int prof)
    {
        int worst = -1;
        List<Integer> studs = d.prof_stud.get(prof);
        List<Integer> studs_reversed = new LinkedList<>();
        for (Integer i: studs)
            studs_reversed.add(0, i);
        // From worst to best
        for (Integer i:studs_reversed)
        {
            // Check if assigned to this professor
            List<Integer> ass = d.a_prof_stud.get(prof);
            for (Integer j: ass)
            {
                if (i.equals(j))
                {
                    worst = i;
                    break;
                }
            }
        }
        return worst;
    };
    
    // Find project to which a student is assigned
    private int projForStud(int stud)
    {
        int proj = -1;
        for (Map.Entry<Integer, List<Integer>> e: d.a_proj_stud.entrySet())
        {
            for (Integer s: e.getValue())
                if (s.equals(stud))
                {
                    proj = e.getKey();
                    break;
                }
        }
        return proj;
    };
    
    // Unassign a student from a project
    private void unassign(int stud, int proj)
    {
        // Remove from project -> student assignments
        List<Integer> l1 = d.a_proj_stud.get(proj);
        Iterator<Integer> l1_i = l1.iterator();
        while (l1_i.hasNext())
        {
            Integer ii = l1_i.next();
            if (ii.equals(stud))
            {
                l1_i.remove();
                break;
            }
        }
        List<Integer> l2 = d.a_prof_stud.get(profForProj(proj));
        Iterator<Integer> l2_i = l2.iterator();
        while (l2_i.hasNext())
        {
            Integer ii = l2_i.next();
            if (ii.equals(stud))
            {
                l2_i.remove();
                break;
            }
        }        
    };
    
    // Delete project from student prefs and delete student from projected preferences of (lecturer, project)
    private void mysteryDelete(int stud, int proj)
    {
        // Delete project from student preferences list
        List<Integer> sp = d.stud_proj.get(stud);
        Iterator<Integer> i_sp = sp.iterator();
        while (i_sp.hasNext())
        {
            Integer i_sp_val = i_sp.next();
            if (i_sp_val.equals(proj))
            {
                i_sp.remove();
                break;
            }
        }
        // And delete student from projected preferences
        int prof = profForProj(proj);
        List<Integer> pp = d.prof_proj_projected.get(prof).get(proj);
        Iterator<Integer> i_pp = pp.iterator();
        while (i_pp.hasNext())
        {
            Integer i_pp_val = i_pp.next();
            if (i_pp_val.equals(proj))
            {
                i_pp.remove();
                break;
            }
        }
    }
    
    // Do the mystery delete on the successors of this student in the projected preferences list
    private void mysteryDeleteSuccessors(int stud, int proj)
    {
        List<Integer> to_delete = new LinkedList<>();
        List<Integer> pp = d.prof_proj_projected.get(profForProj(proj)).get(proj);
        Iterator<Integer> i = pp.iterator();
        while (i.hasNext())
        {
            Integer i_val = i.next();
            if (i_val.equals(stud))
                break;
        }
        while (i.hasNext())
            to_delete.add(i.next());
        for (Integer victim: to_delete)
            mysteryDelete(victim, proj);
    }
    
    // Mystery delete the pairs:
    // (successor of stud in the projected preferences list of prof and proj, project that this prof does and is preferred by stud)
    private void mysteryDeleteSuccessorsForProf(int stud, int prof, int proj)
    {
        List<Integer> to_delete_stud = new LinkedList<>();
        List<Integer> to_delete_proj = new LinkedList<>();
        List<Integer> pp = d.prof_proj_projected.get(profForProj(proj)).get(prof);
        Iterator<Integer> i = pp.iterator();
        while (i.hasNext())
        {
            Integer i_val = i.next();
            if (i_val.equals(stud))
                break;
        }
        while (i.hasNext())
        {
            int s = i.next();
            // Find this student's preferences list
            List<Integer> s_pref = d.stud_proj.get(s);
            // And this professor's projects
            List<Integer> p_proj = d.prof_proj.get(prof);
            // Find intersection
            for (Integer s_pref_i: s_pref)
                for (Integer p_proj_i: p_proj)
                    if (s_pref_i.equals(p_proj_i))
                    {
                        to_delete_stud.add(s);
                        to_delete_proj.add(p_proj_i);
                    }
        }
        Iterator<Integer> tds_i = to_delete_stud.iterator();
        Iterator<Integer> tdp_i = to_delete_proj.iterator();
        while (tds_i.hasNext())
        {
            mysteryDelete(tds_i.next(), tdp_i.next());
        }
    }
    
    public void doSPA(SPAData d)
    {
        this.d = d;
        d.a_prof_stud = new HashMap<>();
        for (int i=0; i<prof_count; ++i)
            d.a_prof_stud.put(i, new LinkedList<Integer>());
        d.a_proj_stud = new HashMap<>();
        for (int i=0; i<proj_count; ++i)
            d.a_proj_stud.put(i, new LinkedList<Integer>());
        d.prof_proj_projected = new HashMap<Integer, Map<Integer, List<Integer>>>();
        buildProjectedLecturerPrefs();
        
        do
        {
            int cand = findUnassignedStudentWithPrefs();
            // No one left unassigned? We're done.
            if (cand == -1)
                break;
            // Student's most preferred project
            int cand_proj = d.stud_proj.get(cand).get(0);
            int cand_prof = profForProj(cand_proj);

            // Local pointers for the lists of students assigned to the current project and prof
            List<Integer> a_proj_studs = d.a_proj_stud.get(cand_proj);
            List<Integer> a_prof_studs = d.a_prof_stud.get(cand_prof);

            // Assign student to project, and to professor who does that project
            a_proj_studs.add(cand);
            a_prof_studs.add(cand);
            
            // Is project over subscribed?
            if (a_proj_studs.size() > d.proj_cap.get(cand_proj))
            {
                int worst_student = worstStudForProj(cand_proj);
                unassign(worst_student, cand_proj);            
            }
            // Is professor over subscribed?
            //TODO: else? Or always? Reread article
            else if (a_prof_studs.size() > d.prof_cap.get(cand_prof))
            {
                int worst_student = worstStudForProf(cand_prof);
                int worst_student_proj = projForStud(worst_student);
                unassign(worst_student, worst_student_proj);
            }
            System.out.println("loop");
            // Is project capped?
            if (a_proj_studs.size() == d.proj_cap.get(cand_proj))
            {
                int worst_student = worstStudForProj(cand_proj);
                mysteryDeleteSuccessors(worst_student, cand_proj);
            }
            // Is professor capped?
            if (a_prof_studs.size() == d.prof_cap.get(cand_prof))
            {
                int worst_student = worstStudForProf(cand_prof);
                mysteryDeleteSuccessorsForProf(worst_student, cand_prof, cand_proj);
            }
            
        } while(true);
        
        System.out.println("Assignments: Project - Students");
        for (Map.Entry<Integer, List<Integer>> i: d.a_proj_stud.entrySet())
        {
            System.out.print("aps: " + i.getKey() + " -");
            for (Integer j: i.getValue())
                System.out.print(" " + j);
            System.out.println();
        }
    };
    
    public static void main(String[] args)
    {
        SPAData d = new SPAData();
        d.stud_count = stud_count;
        d.prof_count = prof_count;
        d.proj_count = proj_count;
        d.proj_cap = new LinkedList<Integer>();
        for (int i: proj_cap)
            d.proj_cap.add(i);
        d.prof_cap = new LinkedList<Integer>();
        for (int i: prof_cap)
            d.prof_cap.add(i);
        d.stud_proj = new HashMap<Integer, List<Integer>>();
        for (int i=0; i<stud_count; ++i)
        {
            List<Integer> tmp = new LinkedList<Integer>();
            for (int j: stud_proj[i])
            {
                System.out.println("stud_proj: " + i + ", " + j);
                tmp.add(j);
            }
            d.stud_proj.put(i, tmp);
        }
        d.prof_proj = new HashMap<Integer, List<Integer>>();
        for (int i=0; i<prof_count; ++i)
        {
            List<Integer> tmp = new LinkedList<Integer>();
            for (int j: prof_proj[i])
            {
                System.out.println("prof_proj: " + i + ", " + j);
                tmp.add(j);
            }
            d.prof_proj.put(i, tmp);
        }
        d.prof_stud = new HashMap<Integer, List<Integer>>();
        for (int i=0; i<prof_count; ++i)
        {
            List<Integer> tmp = new LinkedList<Integer>();
            for (int j: prof_stud[i])
            {
                System.out.println("prof_stud: " + i + ", " + j);
                tmp.add(j);
            }
            d.prof_stud.put(i, tmp);
        }

        SPAStandalone ss = new SPAStandalone();
        ss.doSPA(d);
        
        // TODO code application logic here
        System.out.println("Command line, sweet command line");
        
        
    }
    
}
