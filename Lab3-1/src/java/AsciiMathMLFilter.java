import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;


public class AsciiMathMLFilter implements Filter {
    
    private static final boolean debug = true;

    private String searchParam;
    private String replaceParam;
    
    public AsciiMathMLFilter() {
    }    
    
    public void init(FilterConfig filterConfig) {                    
        searchParam = filterConfig.getInitParameter("search");
        replaceParam = filterConfig.getInitParameter("replace");    
        log("Params Search: " + searchParam + " Replace: " + replaceParam);
    }

    
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("AsciiMathMLFilter:doFilter()");
        }
        
        PrintWriter out = response.getWriter();
        ResponseWrapper wrapper = new ResponseWrapper((HttpServletResponse) response);
        
        chain.doFilter(request, wrapper);
            
        String modifiedHtml = wrapper.toString();
            
        String matchHead = "</head>";
        String jsInclude = "<script language='javascript' src='/ASCIIMathML.js'></script>\n";

         if (searchParam != null && replaceParam != null) {
            log("Search: " + searchParam + " Replace: " + replaceParam);
            Pattern searchPattern = Pattern.compile(searchParam);
            Matcher searchMatch = searchPattern.matcher(modifiedHtml);
            modifiedHtml = searchMatch.replaceAll(replaceParam);
        
            int index = modifiedHtml.indexOf(matchHead) - 1;
            if (index > 0)
                modifiedHtml = new StringBuilder(modifiedHtml).insert(index, jsInclude).toString();
        }

        response.setContentLength(modifiedHtml.getBytes().length);
        out.write(modifiedHtml);
        out.flush();
        out.close();          
    }

    public void destroy() {        
    }

    
    public void log(String msg) {
        System.err.println(msg);
    }
    
}
