
import java.io.PrintWriter;
import javax.servlet.ServletOutputStream;

import java.io.CharArrayWriter;
import java.io.DataOutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class ResponseWrapper extends HttpServletResponseWrapper {
    private CharArrayWriter output; //DataOutputStream
    private int contentLength;
    private String contentType;
 

    public ResponseWrapper(HttpServletResponse response) {
        super(response);
        this.output = new CharArrayWriter();
    }

    public String toString() {
        return output.toString();
    }

    public PrintWriter getWriter() {
        return new PrintWriter(output);
    }
    
    public void setContentLength(int length) { 
        this.contentLength = length;
        super.setContentLength(length); 
    } 
 
    public int getContentLength() { 
        return contentLength; 
    } 
 
    public void setContentType(String type) { 
        this.contentType = type;
        super.setContentType(type); 
    } 
 
  public String getContentType() { 
    return contentType; 
  } 
}
