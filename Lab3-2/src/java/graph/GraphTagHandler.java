/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author torp
 */
public class GraphTagHandler extends SimpleTagSupport implements DynamicAttributes
{
    private Map<String, Object> attrs = new HashMap<String, Object>();
    
    public void setDynamicAttribute(String uri, String name, Object value) throws JspException
    {
        attrs.put(name, value);
    }
    
    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        
        for (Map.Entry<String, Object> entry : attrs.entrySet())
        {
            out.print("attr name: " + entry.getKey() + " value: " + (String)entry.getValue());
        }
        
        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");

            JspFragment f = getJspBody();
            if (f != null)
            {
                StringWriter cucu = new StringWriter();
                f.invoke(cucu);
                String mamaliga = cucu.toString();
                Graph<Integer, String> gg = new SparseMultigraph<>();
                String[] lines = mamaliga.split("\n");
                boolean parsing_vertexes = true;
                for(String l: lines)
                {
                    out.print("Line: " + l + "</br>");
                    if (parsing_vertexes)
                    {
                        if (l.startsWith("#"))
                        {
                            parsing_vertexes = false;
                        }
                        else
                        {
                            String[] items = l.split(" ");
                            out.print("items.size=" + items.length + "</br>");
                            try
                            {
                                Integer tmp = Integer.parseInt(items[0]);
                                gg.addVertex(tmp);
                                out.print("Vertex: " + tmp + " </br>");
                            }
                            catch (Exception e)
                            {
                                // Just skip lines i cannot parse
                            }
                        }
                    }
                    else
                    {
                        // Parse edges here
                        String[] items = l.split(" ");
                        try
                        {
                            Integer tmp = Integer.parseInt(items[0]);
                            Integer tmp2 = Integer.parseInt(items[1]);
                            gg.addEdge("Edge", tmp, tmp2);
                            out.print("Edge: " + tmp + "-" + tmp2 + " </br>");
                        }
                        catch (Exception e)
                        {
                            // Again, just skip what i cannot understand
                        }
                    }
                }
                out.print(gg.toString());
            }

            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in GraphTagHandler tag", ex);
        }
    }
    
}
