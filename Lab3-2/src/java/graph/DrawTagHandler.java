package graph;

import java.io.StringWriter;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class DrawTagHandler extends SimpleTagSupport
{
    private String colour = "black";
    private String layout = "random";
    
    public String getColour()
    {
        return colour;
    }
    
    public void setColour(String val)
    {
        colour = val;
    }
    
    public String getLayout()
    {
        return layout;
    }
    
    public void setLayout(String val)
    {
        layout = val;
    }
    
    @Override
    public void doTag() throws JspException
    {
        JspWriter out = getJspContext().getOut();
                
        try {
            out.print("<div>BEGIN TAG draw</div>");
            out.print("<div> attr colour = " + colour + "</div>");
            out.print("<div> attr layout = " + layout + "</div>");
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");

            JspFragment f = getJspBody();
            if (f != null)
            {
                f.invoke(out);
            }

            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
            out.print("<div>END TAG draw</div>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in GraphTagsHandler tag", ex);
        }
    }    
}
