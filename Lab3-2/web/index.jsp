<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <%@ taglib uri="/WEB-INF/tlds/tagtest" prefix="say" %>
        <%@ taglib uri="/WEB-INF/tlds/GraphTags" prefix="g" %>
        <div>JSP begins here</div>
        <div><say:test who="Ponta"/></div>
        <div><g:draw>
                <g:graph file="cucu.tgf">
                    1 First node
                    2 Second node
                    3 
                    #
                    1 2 Edge between the two
                    2 3
                </g:graph>
            </g:draw></div>
    </body>
</html>
