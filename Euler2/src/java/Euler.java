/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.math.BigDecimal;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Euler"})
public class Euler extends HttpServlet {
    
    private BigDecimal mostPreciseNumber;
    private static final String fileName = "number.txt";
    private String fullFileName = null;
    
    public void init() throws ServletException {
        super.init();
        ServletContext context = getServletConfig().getServletContext();
        fullFileName = context.getRealPath("/WEB-INF/" + fileName);
        mostPreciseNumber = readMostPreciseNumber();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int requestedPrecision = Integer.parseInt(request.getParameter("precision"));
        int oldPrecision = mostPreciseNumber.precision();
        
        if (requestedPrecision > mostPreciseNumber.precision())
        {
            mostPreciseNumber = EulerNumber.euler(requestedPrecision);
            saveMostPreciseNumber(mostPreciseNumber);
        }
        
        response.setContentType("text/plain");
        PrintWriter r = response.getWriter();
        r.println("Euler Number: " + mostPreciseNumber + " Requested/Old Precision:" + requestedPrecision + "/" + oldPrecision);
    }

    @Override
    public String getServletInfo() {
        return "Lab1 Euler";
    }
    
    private BigDecimal readMostPreciseNumber()
    {
       BigDecimal n;
       try {
            FileReader f = new FileReader(fileName);
            BufferedReader b = new BufferedReader(f);
            String l = b.readLine();
            n = new BigDecimal(l);
            return n;
        }
        // I don't care why it failed, let's have a zero decimals default
        catch (Exception e)
        {
            return new BigDecimal(2);
        }
    }
    
    private synchronized void saveMostPreciseNumber(BigDecimal n)
    {
        try {
          FileWriter f = new FileWriter(fileName);
          String s = n.toPlainString();
          f.write(s, 0, s.length());
          f.close();
          return;
        }
        catch (IOException ignored) {}
    }
}
