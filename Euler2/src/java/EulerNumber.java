import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

public class EulerNumber {

    static private HashMap<Integer, BigInteger> computed = new HashMap<Integer, BigInteger>();
    // What max precision, time it out baby!
    // static public final int MAX_PRECISION = 1000;

    public static BigInteger factorial(int n)
    {
	BigInteger l;
	if (n == 0) return BigInteger.ONE;
        
        /*
        l = computed.get(n);
        
	if (l != null)
	    return l;
        */
	l = BigInteger.valueOf(n).multiply(factorial(n - 1));

	//computed.put(n, l);

	return l;
    }

    public static BigDecimal euler(int precision)
    {
	BigDecimal e = BigDecimal.ONE;

	if (precision < 1) precision = 1;
	//if (precision > MAX_PRECISION) precision = MAX_PRECISION;

	for (int i = 1; i < precision; i++)
	{
            BigInteger f = factorial(i);
	    e = e.add(BigDecimal.ONE.divide(new BigDecimal(f), precision, BigDecimal.ROUND_FLOOR));
	}

	return e;
    }
}
