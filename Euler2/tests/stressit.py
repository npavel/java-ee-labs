#!/usr/bin/python
import sys
from threading import Thread
import pycurl
import cStringIO

def onereq(num, reqnum):
    buf = cStringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, "http://localhost:8080/Euler2/Euler?precision=10000")
    c.setopt(c.WRITEFUNCTION, buf.write)
    try:
        c.perform()
    except:
        print num, reqnum, "server timed out"
    else:
        print num, reqnum, "status", c.getinfo(pycurl.HTTP_CODE)

def onethread(num):
    print num, "starting"
    for i in range(1, 6):
	onereq(num, i)
    print num, "finished"

threads = []
for t in range(1, 151):
    tt = Thread(target=onethread, args = (t, ))
    threads.append(tt)
    tt.start()

for ttt in threads:
    ttt.join()
