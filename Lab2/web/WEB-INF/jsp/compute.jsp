
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="userbean" scope="session" class="model.UserBean" />
<jsp:useBean id="computebean" scope="session" class="model.ComputeBean" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compute</title>
    </head>
    <body>
          
        <div style="font-family: Arial; font-size: 14px;"> 
        <h2>Hello <jsp:getProperty name="userbean" property="username"/> (<a href="/?action=logout">Logout</a>)</h2>
            <%@ include file="/html/compute.html" %>
        </div>
    </body>
</html>
