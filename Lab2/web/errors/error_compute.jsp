<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="computeBean" scope="session" class="model.ComputeBean"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Result</title>
    </head>
    <body>
        <div style="font-family: Arial; font-size: 14px;"> 
        <h1>Invalid expression:</h1>
        <h2>
        <jsp:getProperty name="computeBean" property="number1"/>
        <jsp:getProperty name="computeBean" property="operator"/>
        <jsp:getProperty name="computeBean" property="number2"/>
        </h2>
        <br><br>Perform another <a href="/compute.jsp">computation</a>
        </div>
    </body>
</html>
