/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ComputeBean;

public class ComputeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession(false);
        ComputeBean computeBean = (ComputeBean) session.getAttribute("computeBean");
        
        if (computeBean != null) {
            BigDecimal n1 = computeBean.getNumber1AsBigDecimal();
            BigDecimal n2 = computeBean.getNumber2AsBigDecimal();
            Character o = computeBean.getOperator().charAt(0);
            
            BigDecimal result = computeExpression(n1, n2, o);
            
            if (result != null) {
                computeBean.setResult(result);
                response.sendRedirect("/result.jsp");
            } else {
                response.sendRedirect("/errors/error_compute.jsp");
            }

        } else {
            response.sendRedirect("/");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
    private BigDecimal computeExpression(BigDecimal n1, BigDecimal n2, Character o)
    {
        switch(o) {
            case '+': return n1.add(n2);
            case '-': return n1.subtract(n2);
            case '*': return n1.multiply(n2);
            case '/': return n1.divide(n2);
            default:  return null;
        }
    }

}
