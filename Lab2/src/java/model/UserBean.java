package model;

public class UserBean {
    private String username;
    private String password;
    private boolean rememberMe;
    private String captcha;

    public String getUsername() {
        return this.username;
    }
        
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getCaptcha() {
        return this.captcha;
    }
        
    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public void setRememberMe(boolean remember) {
        rememberMe = remember;
    }
    
    public boolean getRememberMe() {
        return rememberMe;
    }  
}
