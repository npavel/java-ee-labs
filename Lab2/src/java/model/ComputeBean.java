
package model;

import java.math.BigDecimal;
import javax.ejb.Stateless;

@Stateless
public class ComputeBean {
    private String number1;
    private String number2;
    private String operator;
    
    private BigDecimal result; 
    
    public String getNumber1() {
        return number1;
    }
    
    public BigDecimal getNumber1AsBigDecimal() {
        return new BigDecimal(number1);
    }
            
    public void setNumber1(String str) {
        number1 = str;
    }

    public String getNumber2() {
        return number2;
    }
    
    public BigDecimal getNumber2AsBigDecimal() {
        return new BigDecimal(number2);
    }
    
    public void setNumber2(String str) {
        number2 = str;
    }
    
    public String getOperator() {
        return operator;
    }
    
    public void setOperator(String str) {
        operator = str;
    }
  
    public BigDecimal getResult() {
        return result;
    }
    
    public void setResult(BigDecimal n) {
        result = n;
    }           
}
