import model.UserBean;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

public class FrontController extends HttpServlet {

    private final String jspPath = "/WEB-INF/jsp";
    LoginService loginService = new LoginService();
    private LoginService.LoginStatus loginStatus = LoginService.LoginStatus.UNKNOWN;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        UserBean userBean = (UserBean) session.getAttribute("userbean");
        String captcha = (String) session.getAttribute("captcha");
        String resource = request.getPathInfo();
        String action = request.getParameter("action");
        
        
        if (hasLoginCookie(request)) {
            loginStatus = LoginService.LoginStatus.COOKIEOK;
        } else {
            loginStatus = loginService.checkLogin(userBean, captcha);
            if (loginStatus == LoginService.LoginStatus.OK && loginService.shouldRememberLogin())
                    setRememberMe(response, loginService.getLoggedUser());                        
        }
        
        System.err.println("Login status: " + loginStatus);
        
        if (action != null && action.equals("logout")) {
            logout(session, response);
            response.sendRedirect("/");
            return;
        }

        
        switch(loginStatus) {
            case OK:
            case ALREADYLOGGED:
            case COOKIEOK:                
                resource = jspPath + "/compute.jsp";                
                break;
            case AUTHFAILED:
                resource = "/errors/error.jsp";
                break;
            case INVALIDCAPTCHA:
                resource = "/errors/error_captcha.jsp";
                break;
            case UNKNOWN:
                resource = "/login.jsp";
                break;
            case INVALIDUSER:
                resource = "/register.jsp";
                break;
            default:
                resource = "/login.jsp";
        }
        System.err.println("Resource " + resource);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(resource);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Lab 2";
    }
    
    private boolean hasLoginCookie(HttpServletRequest request) {
        if (request == null)
            return false;
        
        Cookie[] cookies = request.getCookies(); 
        UUID uuid = null;
        String username = null;
        
        if (cookies == null)
            return false;
        for(int i = 0; i < cookies.length; i++) { 
            Cookie c = cookies[i];
            String n = c.getName();
            if (n.equals("loginUUID")) {
                uuid = UUID.fromString(c.getValue());
            }
            if (n.equals("loginUser")) {
                username = c.getValue();
            }
        }
        
        if (uuid != null && username != null) {
            System.err.println("Found login cookie " + username + " : " + uuid);
            return true;
        }
        
        return false;
    }  
    
    private void setRememberMe(HttpServletResponse response, String username) {
        UUID uuid = UUID.randomUUID();
        Cookie loginUUID = new Cookie("loginUUID", uuid.toString());
        Cookie loginUser = new Cookie("loginUser", username);
        
        loginUUID.setMaxAge(30 * 24 * 60 * 60); //~1 month
        loginUser.setMaxAge(30 * 24 * 60 * 60); //~1 month
        
        loginUser.setPath("/");
        loginUUID.setPath("/");
        
        response.addCookie(loginUUID);
        response.addCookie(loginUser);
        System.err.println("Setting remember me cookie");
    }
    
    private void logout(HttpSession session, HttpServletResponse response) {
        //remove beans
        session.removeAttribute("userbean");
        session.removeAttribute("computeBean");

        //remove autologin cookie
        Cookie logout = new Cookie("loginUser", "");
        logout.setMaxAge(0);
        logout.setPath("/");
        response.addCookie(logout);
        System.err.println("Removing cookie");
        
        loginService.logout();
        loginStatus = LoginService.LoginStatus.UNKNOWN;
    }
}
