import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.List;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;

import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.*;
import javax.servlet.http.*;

public class CaptchaServlet extends HttpServlet {

    public final int IMG_WIDTH = 200;
    public final int IMG_HEIGHT = 40;
    public final int CAPTCHA_LEN = 10;
    private final String CAPTCHA_CHARS = "qwertyuiopasdfghjklzxcvbnm";
    
    private StringHash hasher = new StringHash();

    public CaptchaServlet() {
    }

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        BufferedImage bufferedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT,
                BufferedImage.TYPE_INT_RGB);

        Graphics2D g2d = bufferedImage.createGraphics();

        Font font = new Font("SansSerif", Font.BOLD, 16);
        g2d.setFont(font);
        g2d.fillRect(0, 0, IMG_WIDTH, IMG_HEIGHT);

        String captcha = generateCaptchaString(CAPTCHA_CHARS, CAPTCHA_LEN);
        String captchaHash = new StringHash().hashString(captcha);
        
        String mess = generateCaptchaString(CAPTCHA_CHARS, CAPTCHA_LEN);
        request.getSession().setAttribute("captcha", captchaHash);

        int x = 0;
        int y = 0;
        Random rng = new Random();
        
        //Messing Captcha
        //g2d.setColor(new Color(80, 183, 0));
        g2d.setColor(new Color(rc(rng, 130, 250), rc(rng, 130, 250), rc(rng, 130, 250)));
        for (int i = 0; i < CAPTCHA_LEN; i++) {
            x += (Math.abs(rng.nextInt()) % 15) + 10;
            y = (Math.abs(rng.nextInt()) % 20) + 20;
            g2d.drawChars(mess.toCharArray(), i, 1, x, y);
        }
        
        //Real captcha (dark color)
        //g2d.setColor(new Color(0, 0, 0));
        g2d.setColor(new Color(rc(rng, 0, 100), rc(rng, 0, 100), rc(rng, 0, 100)));
        x = y = 0;
        for (int i = 0; i < CAPTCHA_LEN; i++) {
            x += (Math.abs(rng.nextInt()) % 8) + 10;
            y = (Math.abs(rng.nextInt()) % 20) + 20;
            g2d.drawChars(captcha.toCharArray(), i, 1, x, y);
        }        

        g2d.dispose();

        response.setContentType("image/png");
        OutputStream os = response.getOutputStream();
        ImageIO.write(bufferedImage, "png", os);
        os.close();
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private String generateCaptchaString(String availableChars, int length) {
        Random rng = new Random();
        StringBuilder s = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            s.append(availableChars.charAt(rng.nextInt(availableChars.length())));
        }
        return s.toString();
    }
    
    private int rc(Random rng, int min, int max) {
        return rng.nextInt((max - min) + 1) + min;
    }
}
