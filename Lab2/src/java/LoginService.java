
import model.UserBean;

public class LoginService {
    
    public enum LoginStatus {
        ALREADYLOGGED,
        AUTHFAILED, 
        INVALIDUSER, 
        OK,
        COOKIEOK,
        INVALIDCAPTCHA,
        UNKNOWN
    };
    
    private boolean isLogged = false;
    private String loggedUser = null;
    private boolean rememberLogin = false;
    
    private StringHash hasher = new StringHash();
    
    public LoginStatus checkLogin(UserBean userBean, String captchaHash) {
        
        if (isLogged)
            return LoginStatus.ALREADYLOGGED;
        
        if (userBean == null) {
            System.err.println("No UserBean data");
            return LoginStatus.UNKNOWN;            
        } 
        
        
        String username = userBean.getUsername();
        String password = userBean.getPassword();
        String captcha = userBean.getCaptcha();
        
        if (!captchaHash.equals(hasher.hashString(captcha))) {
            System.err.println("Invalid captcha");
            return LoginStatus.INVALIDCAPTCHA;
        }
        
        if (username == null || password == null) {
            System.err.println("invalid user/password");
            return LoginStatus.AUTHFAILED;
        }
        
        if (username.equals("torp") || username.equals("panic") || username.equals("acf")) {
                isLogged = true;
                loggedUser = username;
                rememberLogin = userBean.getRememberMe();
                return LoginStatus.OK;
                
        } else {
                return LoginStatus.INVALIDUSER;
        }
    } 
    
    public String getLoggedUser() {
        if (isLogged) {
            return loggedUser;
        } else {
            return null;
        }           
    }
    
    public boolean shouldRememberLogin() {
        return rememberLogin;
    }
    
    public void logout() {
        isLogged = false;
        loggedUser = null;
    }
}
