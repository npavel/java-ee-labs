package model;

import java.io.Serializable;

public class StudentPreference implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private int score;
    private Student studentId;
    private Project projectId;
    public StudentPreference() {
    }

    public StudentPreference(Integer id) {
        this.id = id;
    }

    public StudentPreference(Integer id, int score) {
        this.id = id;
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Student getStudentId() {
        return studentId;
    }

    public void setStudentId(Student studentId) {
        this.studentId = studentId;
    }

    public Project getProjectId() {
        return projectId;
    }

    public void setProjectId(Project projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentPreference)) {
            return false;
        }
        StudentPreference other = (StudentPreference) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.StudentPreference[ id=" + id + " ]";
    }
    
}
