package model;

import java.io.Serializable;
import java.util.List;

public class Project implements Serializable {
    private static final long serialVersionUID = 1L;    
    private Integer id;   
    private String name;    
    private Integer capacity;
    private String description;
    private List<StudentPreference> studentPreferenceList;
    private Professor professorId;

    public Project() {
    }

    public Project(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<StudentPreference> getStudentPreferenceList() {
        return studentPreferenceList;
    }

    public void setStudentPreferenceList(List<StudentPreference> studentPreferenceList) {
        this.studentPreferenceList = studentPreferenceList;
    }

    public Professor getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Professor professorId) {
        this.professorId = professorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Project[ id=" + id + " ]";
    }
    
}
