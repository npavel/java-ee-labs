
package dao;

public abstract class DAOAbstractFactory 
{
    abstract DAOGeneric getJDBC(String className);
    abstract DAOGeneric getJPA(String className);
    abstract DAOGeneric getODB(String className);   
}
