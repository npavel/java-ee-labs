package dao.JDBC;

import dao.DAOException;
import dao.DAOGeneric;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.Student;

public class DAOStudentJDBC implements DAOGeneric {
    
    //@Resource(mappedName="jdbc/spa")
       
    private DataSource ds;

    public DAOStudentJDBC() {
        try {
            ds = (DataSource) new InitialContext().lookup("jdbc/spa");
        } catch (NamingException e) {
            throw new DAOException("Datasource missing in JNDI");
        }
    }

    @Override
    public Object find(Serializable id) {
       return null;
    }

    @Override
    public List find() {
        List<Student> list = new ArrayList<>();
        
        try {
            if(ds == null)
                throw new SQLException("Can't get data source");

            Connection con = ds.getConnection();

            if(con == null)
                throw new SQLException("Can't get database connection");

            PreparedStatement ps = con.prepareStatement(
                    "select id, firstname, lastname, email from students"
            );
            
            ResultSet result =  ps.executeQuery();

            while(result.next()) {
                Student tmpStud = new Student();
                
                tmpStud.setId(result.getLong("id"));
                tmpStud.setFirstName(result.getString("firstname"));
                tmpStud.setLastName(result.getString("lastname"));
                tmpStud.setEmail(result.getString("email"));
                
                list.add(tmpStud);
            }
            
            con.close();

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(DAOStudentJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (null);
    }

    @Override
    public void save(Object value) {
        Student student = (Student) value;
        
        try {
            if(ds == null)
                throw new SQLException("Can't get data source");
            
            Connection con = ds.getConnection();
            
            if(con == null)
                throw new SQLException("Can't get database connection");
            
            PreparedStatement ps = con.prepareStatement(
                    "insert into students (firstname, lastname, email)VALUES (?,?,?)"
            );
            
            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setString(3, student.getEmail());
            
            int affectedRows = ps.executeUpdate();
            
            if (affectedRows == 0) {
                throw new SQLException("Creating student failed, no rows affected.");
            }
            
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys != null && generatedKeys.next()) {
                    student.setId(generatedKeys.getLong(1));
                } else {
                    student.setId(-1);
                }
            }
            finally {
                con.close();
            }
            
            System.err.println("Added: " + student.getFirstName() + " " + student.getLastName());
        }
        catch (SQLException ex) {
            Logger.getLogger(DAOStudentJDBC.class.getName()).log(Level.SEVERE, null, ex);            
        }
    }

    @Override
    public void update(Object value) {
    }

    @Override
    public void delete(Object value) {
        Student student = (Student) value;
        try {
            
            if(ds == null)
                throw new SQLException("Can't get data source");

            Connection con = ds.getConnection();

            if(con == null)
                throw new SQLException("Can't get database connection");

            PreparedStatement ps = con.prepareStatement(
                       "delete from students where id=?"
                    ); 
            
            ps.setLong(1, student.getId());
            
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Removing student failed, no rows affected.");
            }
        
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }    
}
