package dao;

import java.io.Serializable;
import java.util.List;

public interface DAOGeneric <O extends Serializable, T>
{
    public T find(O id);
    public List<T> find();
    public void save(T value);
    public void update(T value);
    public void delete(T value);      
}
