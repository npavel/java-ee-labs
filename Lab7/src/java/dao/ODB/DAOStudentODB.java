package dao.ODB;

import dao.DAOGeneric;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import model.Student;

public class DAOStudentODB implements DAOGeneric {

    private EntityManager em;
    
    public DAOStudentODB() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("$objectdb/student.odb");
        em = emf.createEntityManager();
        em.getMetamodel().entity(Student.class);
    }

    
    @Override
    public Object find(Serializable id) {
       return null;
    }

    @Override
    public List find() {
        TypedQuery<Student> query = em.createQuery("SELECT s FROM Student s", Student.class);
        List<Student> list = query.getResultList();
        System.out.println("ODB: Retrieved students");
        return list;
    }

    @Override
    public void save(Object value) {
        Student student = (Student) value;
        em.getTransaction().begin();
        em.persist(student);
        em.getTransaction().commit();
        System.out.println("ODB: Added student: " + student.toString());
    }

    @Override
    public void update(Object value) {
    }

    @Override
    public void delete(Object value) {
        Student student = (Student) value;
        em.getTransaction().begin();
        em.remove(student);
        em.getTransaction().commit();
    }    
}
