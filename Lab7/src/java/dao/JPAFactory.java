
package dao;

import dao.JPA.DAOProfessorJPA;
import dao.JPA.DAOProjectJPA;
import dao.JPA.DAOStudentJPA;
import dao.JPA.DAOStudentPreferenceJPA;


public class JPAFactory extends DAOAbstractFactory {

    @Override
    DAOGeneric getJDBC(String className) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    DAOGeneric getJPA(String className) {
          if (className == null)
            return null;
        
        if (className.equalsIgnoreCase("student"))
            return new DAOStudentJPA();
        
        if (className.equalsIgnoreCase("professor"))
            return new DAOProfessorJPA();
        
        if (className.equalsIgnoreCase("project"))
            return new DAOProjectJPA();
        
        if (className.equalsIgnoreCase("studentpreference"))
            return new DAOStudentPreferenceJPA();
        
        return null;
    }

    @Override
    DAOGeneric getODB(String className) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
