
package dao;

import dao.ODB.DAOProfessorODB;
import dao.ODB.DAOProjectODB;
import dao.ODB.DAOStudentODB;
import dao.ODB.DAOStudentPreferenceODB;

public class ODBFactory extends DAOAbstractFactory {

    @Override
    DAOGeneric getJDBC(String className) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    DAOGeneric getJPA(String className) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    DAOGeneric getODB(String className) {
        if (className == null)
            return null;
        
        if (className.equalsIgnoreCase("student"))
            return new DAOStudentODB();
        
        if (className.equalsIgnoreCase("professor"))
            return new DAOProfessorODB();
        
        if (className.equalsIgnoreCase("project"))
            return new DAOProjectODB();
        
        if (className.equalsIgnoreCase("studentpreference"))
            return new DAOStudentPreferenceODB();
        
        return null;
    }
    
}
