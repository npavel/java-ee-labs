package dao;

public class DAOFactory {
    public static DAOAbstractFactory get(String DAOType) {
        if (DAOType.equalsIgnoreCase("jdbc"))
            return new JDBCFactory();
        
        if (DAOType.equalsIgnoreCase("jpa"))
            return new JPAFactory();
        
         if (DAOType.equalsIgnoreCase("odb"))
            return new ODBFactory();
         
        return null;
    }    
}
