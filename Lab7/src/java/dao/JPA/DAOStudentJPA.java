package dao.JPA;

import dao.DAOGeneric;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import model.Student;

public class DAOStudentJPA implements DAOGeneric {
    
    @PersistenceContext(unitName = "Lab7PU")
    private EntityManager em;

    public DAOStudentJPA() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("spa__pm");
        em = emf.createEntityManager();
        em.getMetamodel().entity(Student.class);
    }        

    @Override
    public Object find(Serializable id) {
       return null;
    }

    @Override
    public List find() {
        javax.persistence.criteria.CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Student.class));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void save(Object value) {
        Student student = (Student) value;
        em.getTransaction().begin();
        em.persist(student);
        em.getTransaction().commit();
        System.out.println("ODB: Added student: " + student.toString());
    }

    @Override
    public void update(Object value) {
    }

    @Override
    public void delete(Object value) {
    }    

    private Object getEntityManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
