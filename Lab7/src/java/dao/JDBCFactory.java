package dao;

import dao.JDBC.DAOProfessorJDBC;
import dao.JDBC.DAOProjectJDBC;
import dao.JDBC.DAOStudentJDBC;
import dao.JDBC.DAOStudentPreferenceJDBC;

public class JDBCFactory extends DAOAbstractFactory 
{
    @Override
    public DAOGeneric getJDBC(String className)
    {
        if (className == null)
            return null;
        
        if (className.equalsIgnoreCase("student"))
            return new DAOStudentJDBC();
        
        if (className.equalsIgnoreCase("professor"))
            return new DAOProfessorJDBC();
        
        if (className.equalsIgnoreCase("project"))
            return new DAOProjectJDBC();
        
        if (className.equalsIgnoreCase("studentpreference"))
            return new DAOStudentPreferenceJDBC();
        
        return null;
    }

    @Override
    DAOGeneric getJPA(String className) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    DAOGeneric getODB(String className) {
        throw new UnsupportedOperationException("Not supported.");
    }
}
