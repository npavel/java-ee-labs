package dao;

import java.util.ArrayList;
import java.util.List;
import model.Student;

public class DAOTest {
    
    private static <T> void printList(List<T> list) {
     for (T s: list) {
            System.out.println(s.toString());
        }        
    }
            
    public static void main(String[] args) {
        
        Student student = new Student();
        List<Student> list = new ArrayList<>();
        
        student.setEmail("Veritus@java.com");
        student.setFirstName("Veritus");
        student.setLastName("Vincitus");
        
        DAOAbstractFactory jdbcDAO = DAOFactory.get("jdbc");
        DAOAbstractFactory odbDAO = DAOFactory.get("odb");
        DAOAbstractFactory jpaDAO = DAOFactory.get("jpa");
        
        DAOGeneric studentDAO;
        
        studentDAO = odbDAO.getODB("student");
        
        studentDAO.save(student);       
        list = studentDAO.find();        
        printList(list);
              
        studentDAO.delete(student);
        
        /*
        studentDAO = jpaDAO.getJPA("student");
        studentDAO.save(student);        
        */
        
        studentDAO = jdbcDAO.getJDBC("student");
        
        studentDAO.save(student);
        list = studentDAO.find();
        printList(list);        
    }
}
