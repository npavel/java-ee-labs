package model.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.Professor;
import model.Student;
import model.Project;


@ManagedBean(name="ProjectBean")
@SessionScoped
public class projectBean implements Serializable {
    
    @Resource(mappedName="jdbc/spa")
    private DataSource ds;
    private List<Project> list = new ArrayList<>();
    private Project project = new Project();

    public List<Project> getList() {
        return list;
    }

    public void setList(List<Project> list) {
        this.list = list;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Project> getProjectList() throws SQLException {
 
        if(ds == null)
                throw new SQLException("Can't get data source");

        Connection con = ds.getConnection();

        if(con == null)
                throw new SQLException("Can't get database connection");

        PreparedStatement ps = con.prepareStatement(
                   "select id, name, description, professor_id, capacity from projects"
                ); 

        ResultSet result =  ps.executeQuery();

        list = new ArrayList<>();

        while(result.next()){
                Project project = new Project(); 

                project.setId(result.getLong("id"));
                project.setName(result.getString("name"));
                project.setDesc(result.getString("description"));
                project.setIdProfessor(result.getInt("professor_id"));
                project.setCapacity(result.getInt("capacity"));

                list.add(project);
        }
        con.close();
        return list;
    }
    
    public void add() throws Exception {
        
        if(ds == null)
            throw new SQLException("Can't get data source");

        Connection con = ds.getConnection();

        if(con == null)
            throw new SQLException("Can't get database connection");

        PreparedStatement ps = con.prepareStatement(
                   "insert into projects (name, capacity, description, professor_id)VALUES (?,?,?,?)"
                ); 
        
        ps.setString(1, project.getName());
        ps.setInt(2, project.getCapacity());
        ps.setString(3, project.getDesc());
        ps.setLong(4, project.getIdProfessor());
        
        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating project failed, no rows affected.");
        }
        
        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys != null && generatedKeys.next()) {
                project.setId(generatedKeys.getLong(1));
            } else {
                project.setId(-1);
            }            
        }
        finally {
            con.close();
        }
        
        list.add(project);
        System.err.println("Added: " + project.getName() + " " + project.getIdProfessor());
    }
    
    public void remove(Project j) {
        
        try {
            list.remove(j);
            
            if(ds == null)
                throw new SQLException("Can't get data source");

            Connection con = ds.getConnection();

            if(con == null)
                throw new SQLException("Can't get database connection");

            PreparedStatement ps = con.prepareStatement(
                       "delete from projects where id=?"
                    ); 
            
            ps.setLong(1, j.getId());
            
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Removing project failed, no rows affected.");
            }
        
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
}
