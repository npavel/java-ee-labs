package model.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.Professor;

@ManagedBean(name="ProfessorBean")
@SessionScoped
public class professorBean implements Serializable {
    
    @Resource(mappedName="jdbc/spa")
    private DataSource ds;
    List<Professor> list = new ArrayList<>();
    Professor professor = new Professor();

    public List<Professor> getList() {
        return list;
    }

    public void setList(List<Professor> list) {
        this.list = list;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
   
    public List<Professor> getProfessorList() throws SQLException{
 
        if(ds == null)
                throw new SQLException("Can't get data source");

        Connection con = ds.getConnection();

        if(con == null)
                throw new SQLException("Can't get database connection");

        PreparedStatement ps = con.prepareStatement(
                   "select id, firstname, lastname, email, capacity from professors"
                ); 

        ResultSet result =  ps.executeQuery();

        list = new ArrayList<>();

        while(result.next()){
                Professor tmpProfessor = new Professor(); 

                tmpProfessor.setId(result.getLong("id"));
                tmpProfessor.setFirstName(result.getString("firstname"));
                tmpProfessor.setLastName(result.getString("lastname"));
                tmpProfessor.setEmail(result.getString("email"));
                tmpProfessor.setCapacity(result.getInt("capacity"));

                list.add(tmpProfessor);
        }
        con.close();
        return list;
    }
    
    public Professor getProfessorById(long id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == id) {
                return list.get(i);
            }
        }
        return null;
    }
    
    public void add() throws Exception {
        
        if(ds == null)
            throw new SQLException("Can't get data source");

        Connection con = ds.getConnection();

        if(con == null)
            throw new SQLException("Can't get database connection");

        PreparedStatement ps = con.prepareStatement(
                   "insert into professors (firstname, lastname, email, capacity) VALUES (?,?,?, 5)"
                ); 
        
        ps.setString(1, professor.getFirstName());
        ps.setString(2, professor.getLastName());
        ps.setString(3, professor.getEmail());
        
        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating professor failed, no rows affected.");
        }
        
        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys != null && generatedKeys.next()) {
                professor.setId(generatedKeys.getLong(1));
            } else {
                professor.setId(-1);
            }            
        }
        finally {
            con.close();
        }
        
        list.add(professor);
        System.err.println("Added professor: " + professor.getFirstName() + " " + professor.getLastName());
    }
    
    public void remove(Professor p) {
        
        try {
            list.remove(p);
            
            if(ds == null)
                throw new SQLException("Can't get data source");

            Connection con = ds.getConnection();

            if(con == null)
                throw new SQLException("Can't get database connection");

            PreparedStatement ps = con.prepareStatement(
                       "delete from professors where id=?"
                    ); 
            
            ps.setLong(1, p.getId());
            
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Removing professor failed, no rows affected.");
            }
        
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
}
