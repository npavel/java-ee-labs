package model.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.Student;

@ManagedBean(name="StudentBean")
@RequestScoped
public class studentBean implements Serializable {
    @Resource(mappedName="jdbc/spa")
       
    private DataSource ds;
    private List<Student> list = new ArrayList<>();
    private Student student = new Student();
    
    public List<Student> getList() {
        return list;
    }

    public void setList(List<Student> list) {
        this.list = list;
    }
     
    public void setStudent(Student student) {
        this.student = student;
    }
    public Student getStudent() {
        return student;
    }
        
      /*
    public studentBean() {
        try {
                Context ctx = new InitialContext();
                ds = (DataSource)ctx.lookup("jdbc/spa");
        } catch (NamingException e) {
                e.printStackTrace();
        }
    }
    */
    
    public List<Student> getStudentList() throws SQLException{
 
            if(ds == null)
                throw new SQLException("Can't get data source");

            Connection con = ds.getConnection();

            if(con == null)
                throw new SQLException("Can't get database connection");

            PreparedStatement ps = con.prepareStatement(
                       "select id, firstname, lastname, email from students"
                    ); 

            ResultSet result =  ps.executeQuery();

            list = new ArrayList<>();

            while(result.next()) {
                    Student tmpStud = new Student();

                    tmpStud.setId(result.getLong("id"));
                    tmpStud.setFirstName(result.getString("firstname"));
                    tmpStud.setLastName(result.getString("lastname"));
                    tmpStud.setEmail(result.getString("email"));

                    list.add(tmpStud);
            }
            
            con.close();

            return list;
	}
    
   
    public void add() throws Exception {
        
        if(ds == null)
            throw new SQLException("Can't get data source");

        Connection con = ds.getConnection();

        if(con == null)
            throw new SQLException("Can't get database connection");

        PreparedStatement ps = con.prepareStatement(
                   "insert into students (firstname, lastname, email)VALUES (?,?,?)"
                ); 
        
        ps.setString(1, student.getFirstName());
        ps.setString(2, student.getLastName());
        ps.setString(3, student.getEmail());
        
        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating student failed, no rows affected.");
        }
        
        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            if (generatedKeys != null && generatedKeys.next()) {
                student.setId(generatedKeys.getLong(1));
            } else {
                student.setId(-1);
            }            
        }
        finally {
            con.close();
        }
        
        list.add(student);
        System.err.println("Added: " + student.getFirstName() + " " + student.getLastName());
    }
    
    public void remove(Student s) {
        
        try {
            list.remove(s);
            
            if(ds == null)
                throw new SQLException("Can't get data source");

            Connection con = ds.getConnection();

            if(con == null)
                throw new SQLException("Can't get database connection");

            PreparedStatement ps = con.prepareStatement(
                       "delete from students where id=?"
                    ); 
            
            ps.setLong(1, s.getId());
            
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Removing student failed, no rows affected.");
            }
        
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
}
