set schema spa;
insert into STUDENTS (firstname, lastname, email) values ('Nicu', 'Pavel', 'panickiss@gmail.com');
insert into STUDENTS (firstname, lastname, email) values ('Andrei Torp', 'Cojocaru', 'not.torp@something.com');
insert into STUDENTS (firstname, lastname, email) values ('Razvan', 'Udrea', 'manager@amazon.com');

insert into PROFESSORS (firstname, lastname, email, capacity) values ('Cristian', 'Frasinaru', 'acf@infoiasi.ro', 100);
insert into PROFESSORS (firstname, lastname, email, capacity) values ('Sabin', 'Buraga', 'busaco@infoiasi.ro', 10);

insert into PROJECTS (name, capacity, description, professor_id) values ('Java 8 Features', 5, 'Implement Java 8 features in Java 7', 1);
insert into PROJECTS (name, capacity, description, professor_id) values ('Java Base64 Encoding', 2, 'Implement Base64 Encoding in Java', 1);
insert into PROJECTS (name, capacity, description, professor_id) values ('API to API', 4, 'Implement API to API proxy', 2);
insert into PROJECTS (name, capacity, description, professor_id) values ('BuzzWord Compliant Test', 4, 'Implement LinkedIn Crawler to check if a user is buzzword compliant', 2);
