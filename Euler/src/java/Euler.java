/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Euler"})
public class Euler extends HttpServlet {
    
    private double mostPreciseNumber;
    private static final String fileName = "number.txt";
    
    public void init() throws ServletException {
        super.init();
        //mostPreciseNumber = readMostPreciseNumber();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int requestedPrecision = Integer.parseInt(request.getParameter("precision"));
        int oldPrecision = getPrecision(mostPreciseNumber);
        
        //if (requestedPrecision > oldPrecision || mostPreciseNumber <= 0.0) {
            mostPreciseNumber = EulerNumber.euler(requestedPrecision);
        //    saveMostPreciseNumber(mostPreciseNumber);
       // }
        
        response.setContentType("text/plain");
        PrintWriter r = response.getWriter();
        r.println("Euler Number: " + mostPreciseNumber + " Requested/Old Precision:" + requestedPrecision + "/" + oldPrecision);
    }

    @Override
    public String getServletInfo() {
        return "Lab1 Euler";
    }
    
    private double readMostPreciseNumber()
    {
       double n;
       try {
            FileReader f = new FileReader(fileName);
            BufferedReader b = new BufferedReader(f);
            String l = b.readLine();
            n = Double.parseDouble(l);
            return n;
        }
        catch (FileNotFoundException ignored) { }
        catch (IOException ignored) {}
        catch (NumberFormatException ignored) { }
       
       return 0.0;
    }
    
    private synchronized void saveMostPreciseNumber(double n)
    {
        try {
          FileWriter f = new FileWriter(fileName);
          String s = Double.toString(n);
          f.write(s, 0, s.length());
          f.close();
          return;
        }
        catch (IOException ignored) {}
    }
    
    private int getPrecision(double n)
    {
        String s = Double.toString(n);
        return s.length() - s.indexOf(".") - 1;
    }
}
