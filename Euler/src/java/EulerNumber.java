
import java.math.BigInteger;
import java.util.HashMap;

public class EulerNumber {

    static private HashMap<Integer, BigInteger> computed = new HashMap<Integer, BigInteger>();
    static public final int MAX_PRECISION = 1000;

    public static BigInteger factorial(int n)
    {
	BigInteger l;
	if (n == 0) return BigInteger.ONE;
        
        /*
        l = computed.get(n);
        
	if (l != null)
	    return l;
        */
	l = BigInteger.valueOf(n).multiply(factorial(n - 1));

	//computed.put(n, l);

	return l;
    }

    public static double euler(int precision)
    {
	double e = 1.0;

	if (precision < 1) precision = 1;
	if (precision > MAX_PRECISION) precision = MAX_PRECISION;

	for (int i = 1; i < precision; i++)
	{
            BigInteger f = factorial(i);
	    e += 1 / f.doubleValue();
	}

	return e;
    }
}
