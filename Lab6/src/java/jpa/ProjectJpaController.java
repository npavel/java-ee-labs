/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Professor;
import model.Project;
import model.StudentPreference;
import model.exceptions.IllegalOrphanException;
import model.exceptions.NonexistentEntityException;
import model.exceptions.RollbackFailureException;

/**
 *
 * @author panic
 */
public class ProjectJpaController implements Serializable {

    public ProjectJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Project project) throws RollbackFailureException, Exception {
        if (project.getStudentPreferenceCollection() == null) {
            project.setStudentPreferenceCollection(new ArrayList<StudentPreference>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Professor professorId = project.getProfessorId();
            if (professorId != null) {
                professorId = em.getReference(professorId.getClass(), professorId.getId());
                project.setProfessorId(professorId);
            }
            Collection<StudentPreference> attachedStudentPreferenceCollection = new ArrayList<StudentPreference>();
            for (StudentPreference studentPreferenceCollectionStudentPreferenceToAttach : project.getStudentPreferenceCollection()) {
                studentPreferenceCollectionStudentPreferenceToAttach = em.getReference(studentPreferenceCollectionStudentPreferenceToAttach.getClass(), studentPreferenceCollectionStudentPreferenceToAttach.getId());
                attachedStudentPreferenceCollection.add(studentPreferenceCollectionStudentPreferenceToAttach);
            }
            project.setStudentPreferenceCollection(attachedStudentPreferenceCollection);
            em.persist(project);
            if (professorId != null) {
                professorId.getProjectCollection().add(project);
                professorId = em.merge(professorId);
            }
            for (StudentPreference studentPreferenceCollectionStudentPreference : project.getStudentPreferenceCollection()) {
                Project oldProjectIdOfStudentPreferenceCollectionStudentPreference = studentPreferenceCollectionStudentPreference.getProjectId();
                studentPreferenceCollectionStudentPreference.setProjectId(project);
                studentPreferenceCollectionStudentPreference = em.merge(studentPreferenceCollectionStudentPreference);
                if (oldProjectIdOfStudentPreferenceCollectionStudentPreference != null) {
                    oldProjectIdOfStudentPreferenceCollectionStudentPreference.getStudentPreferenceCollection().remove(studentPreferenceCollectionStudentPreference);
                    oldProjectIdOfStudentPreferenceCollectionStudentPreference = em.merge(oldProjectIdOfStudentPreferenceCollectionStudentPreference);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Project project) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Project persistentProject = em.find(Project.class, project.getId());
            Professor professorIdOld = persistentProject.getProfessorId();
            Professor professorIdNew = project.getProfessorId();
            Collection<StudentPreference> studentPreferenceCollectionOld = persistentProject.getStudentPreferenceCollection();
            Collection<StudentPreference> studentPreferenceCollectionNew = project.getStudentPreferenceCollection();
            List<String> illegalOrphanMessages = null;
            for (StudentPreference studentPreferenceCollectionOldStudentPreference : studentPreferenceCollectionOld) {
                if (!studentPreferenceCollectionNew.contains(studentPreferenceCollectionOldStudentPreference)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StudentPreference " + studentPreferenceCollectionOldStudentPreference + " since its projectId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (professorIdNew != null) {
                professorIdNew = em.getReference(professorIdNew.getClass(), professorIdNew.getId());
                project.setProfessorId(professorIdNew);
            }
            Collection<StudentPreference> attachedStudentPreferenceCollectionNew = new ArrayList<StudentPreference>();
            for (StudentPreference studentPreferenceCollectionNewStudentPreferenceToAttach : studentPreferenceCollectionNew) {
                studentPreferenceCollectionNewStudentPreferenceToAttach = em.getReference(studentPreferenceCollectionNewStudentPreferenceToAttach.getClass(), studentPreferenceCollectionNewStudentPreferenceToAttach.getId());
                attachedStudentPreferenceCollectionNew.add(studentPreferenceCollectionNewStudentPreferenceToAttach);
            }
            studentPreferenceCollectionNew = attachedStudentPreferenceCollectionNew;
            project.setStudentPreferenceCollection(studentPreferenceCollectionNew);
            project = em.merge(project);
            if (professorIdOld != null && !professorIdOld.equals(professorIdNew)) {
                professorIdOld.getProjectCollection().remove(project);
                professorIdOld = em.merge(professorIdOld);
            }
            if (professorIdNew != null && !professorIdNew.equals(professorIdOld)) {
                professorIdNew.getProjectCollection().add(project);
                professorIdNew = em.merge(professorIdNew);
            }
            for (StudentPreference studentPreferenceCollectionNewStudentPreference : studentPreferenceCollectionNew) {
                if (!studentPreferenceCollectionOld.contains(studentPreferenceCollectionNewStudentPreference)) {
                    Project oldProjectIdOfStudentPreferenceCollectionNewStudentPreference = studentPreferenceCollectionNewStudentPreference.getProjectId();
                    studentPreferenceCollectionNewStudentPreference.setProjectId(project);
                    studentPreferenceCollectionNewStudentPreference = em.merge(studentPreferenceCollectionNewStudentPreference);
                    if (oldProjectIdOfStudentPreferenceCollectionNewStudentPreference != null && !oldProjectIdOfStudentPreferenceCollectionNewStudentPreference.equals(project)) {
                        oldProjectIdOfStudentPreferenceCollectionNewStudentPreference.getStudentPreferenceCollection().remove(studentPreferenceCollectionNewStudentPreference);
                        oldProjectIdOfStudentPreferenceCollectionNewStudentPreference = em.merge(oldProjectIdOfStudentPreferenceCollectionNewStudentPreference);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = project.getId();
                if (findProject(id) == null) {
                    throw new NonexistentEntityException("The project with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Project project;
            try {
                project = em.getReference(Project.class, id);
                project.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The project with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StudentPreference> studentPreferenceCollectionOrphanCheck = project.getStudentPreferenceCollection();
            for (StudentPreference studentPreferenceCollectionOrphanCheckStudentPreference : studentPreferenceCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Project (" + project + ") cannot be destroyed since the StudentPreference " + studentPreferenceCollectionOrphanCheckStudentPreference + " in its studentPreferenceCollection field has a non-nullable projectId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Professor professorId = project.getProfessorId();
            if (professorId != null) {
                professorId.getProjectCollection().remove(project);
                professorId = em.merge(professorId);
            }
            em.remove(project);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Project> findProjectEntities() {
        return findProjectEntities(true, -1, -1);
    }

    public List<Project> findProjectEntities(int maxResults, int firstResult) {
        return findProjectEntities(false, maxResults, firstResult);
    }

    private List<Project> findProjectEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Project.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Project findProject(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Project.class, id);
        } finally {
            em.close();
        }
    }

    public int getProjectCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Project> rt = cq.from(Project.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
