/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import model.Project;
import model.Student;
import model.StudentPreference;
import model.exceptions.NonexistentEntityException;
import model.exceptions.RollbackFailureException;

/**
 *
 * @author panic
 */
public class StudentPreferenceJpaController implements Serializable {

    public StudentPreferenceJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StudentPreference studentPreference) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Student studentId = studentPreference.getStudentId();
            if (studentId != null) {
                studentId = em.getReference(studentId.getClass(), studentId.getId());
                studentPreference.setStudentId(studentId);
            }
            Project projectId = studentPreference.getProjectId();
            if (projectId != null) {
                projectId = em.getReference(projectId.getClass(), projectId.getId());
                studentPreference.setProjectId(projectId);
            }
            em.persist(studentPreference);
            if (studentId != null) {
                studentId.getStudentPreferenceCollection().add(studentPreference);
                studentId = em.merge(studentId);
            }
            if (projectId != null) {
                projectId.getStudentPreferenceCollection().add(studentPreference);
                projectId = em.merge(projectId);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StudentPreference studentPreference) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            StudentPreference persistentStudentPreference = em.find(StudentPreference.class, studentPreference.getId());
            Student studentIdOld = persistentStudentPreference.getStudentId();
            Student studentIdNew = studentPreference.getStudentId();
            Project projectIdOld = persistentStudentPreference.getProjectId();
            Project projectIdNew = studentPreference.getProjectId();
            if (studentIdNew != null) {
                studentIdNew = em.getReference(studentIdNew.getClass(), studentIdNew.getId());
                studentPreference.setStudentId(studentIdNew);
            }
            if (projectIdNew != null) {
                projectIdNew = em.getReference(projectIdNew.getClass(), projectIdNew.getId());
                studentPreference.setProjectId(projectIdNew);
            }
            studentPreference = em.merge(studentPreference);
            if (studentIdOld != null && !studentIdOld.equals(studentIdNew)) {
                studentIdOld.getStudentPreferenceCollection().remove(studentPreference);
                studentIdOld = em.merge(studentIdOld);
            }
            if (studentIdNew != null && !studentIdNew.equals(studentIdOld)) {
                studentIdNew.getStudentPreferenceCollection().add(studentPreference);
                studentIdNew = em.merge(studentIdNew);
            }
            if (projectIdOld != null && !projectIdOld.equals(projectIdNew)) {
                projectIdOld.getStudentPreferenceCollection().remove(studentPreference);
                projectIdOld = em.merge(projectIdOld);
            }
            if (projectIdNew != null && !projectIdNew.equals(projectIdOld)) {
                projectIdNew.getStudentPreferenceCollection().add(studentPreference);
                projectIdNew = em.merge(projectIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = studentPreference.getId();
                if (findStudentPreference(id) == null) {
                    throw new NonexistentEntityException("The studentPreference with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            StudentPreference studentPreference;
            try {
                studentPreference = em.getReference(StudentPreference.class, id);
                studentPreference.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The studentPreference with id " + id + " no longer exists.", enfe);
            }
            Student studentId = studentPreference.getStudentId();
            if (studentId != null) {
                studentId.getStudentPreferenceCollection().remove(studentPreference);
                studentId = em.merge(studentId);
            }
            Project projectId = studentPreference.getProjectId();
            if (projectId != null) {
                projectId.getStudentPreferenceCollection().remove(studentPreference);
                projectId = em.merge(projectId);
            }
            em.remove(studentPreference);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StudentPreference> findStudentPreferenceEntities() {
        return findStudentPreferenceEntities(true, -1, -1);
    }

    public List<StudentPreference> findStudentPreferenceEntities(int maxResults, int firstResult) {
        return findStudentPreferenceEntities(false, maxResults, firstResult);
    }

    private List<StudentPreference> findStudentPreferenceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StudentPreference.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StudentPreference findStudentPreference(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StudentPreference.class, id);
        } finally {
            em.close();
        }
    }

    public int getStudentPreferenceCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StudentPreference> rt = cq.from(StudentPreference.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
