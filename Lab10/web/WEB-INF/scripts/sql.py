#!/usr/bin/python
import itertools
import random

firstName = [
'Nicu',
'Florin',
'Adi',
'Razvan',
'Andrei',
'Calin',
'Radu',
'Cristi',
'Bogdan',
'Mihai',
'Dragos',
'Mircea',
'Ghita',
'Vasile',
'Ion',
'Stefan'
]

lastName = [
'Nitu',
'Plisu',
'Tihar',
'Popescu',
'Lupescu',
'Mared',
'Colup',
'Ionescu',
'Gherman',
'Greuceanu',
'Jder',
'Rosu',
'Verde',
'Izmail'
]

profLastName = [
'Diaconu',
'Popa',
'Radu',
'Ionescu',
'Serban',
'Matei',
'Mazilescu',
'Gheorghe',
'Nemes',
'Stan',
'Pop',
'Dumitrescu',
'Dima',
'Mihai',
'Ionita',
'Dumitru',
'Marin',
'Tudor',
'Dobre',
'Barbu',
'Nistor',
'Florea',
'Ene',
'Vasile',
'Dinu',
'Georgescu',
'Stoica',
'Popescu',
'Diaconescu',
'Mocanu',
'Oprea',
'Stefanescu',
'Voinea',
'Dochioiu',
'Albu',
'Boariu',
'Tabacu',
'Chitu',
'Manole',
'Cristea',
'Toma',
'Constantin',
'Stanescu',
'Preda',
]
firstNames = [ line.strip().split(" ")[0].title() for line in open("census-derived-all-first.txt")]

#names = [zip(x, lastName) for x in itertools.combinations(firstNames, len(lastName))]
#profNames = [zip(x, profLastName) for x in itertools.combinations(firstNames, len(profLastName))]
studNames = []
profNames = []

for f in firstNames:
    for l in lastName:
	studNames.append((f, l))


for f in firstNames:
    for l in profLastName:
	profNames.append((f, l))

with open("insertDataBulkStudents.sql", "w") as sqlFile:
    for n in studNames:
	sqlStr = "insert into STUDENTS (firstname, lastname, email) values ('%s', '%s', '%s');\n" % (n[0], n[1], n[0]+n[1]+"@email.ro")
	sqlFile.write(sqlStr)


with open("insertDataBulkProfessors.sql", "w") as sqlFile:
    for n in profNames:
	sqlStr = "insert into PROFESSORS (firstname, lastname, email, capacity) values ('%s', '%s', '%s', %d);\n" % (n[0], n[1], n[0]+n[1]+"@infoiasi.ro", random.randrange(3, 10))
	sqlFile.write(sqlStr)

#print firstNames
#print studNames
print len(profNames)
print len(studNames)