/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Student;
import model.StudentPreference;
import model.exceptions.IllegalOrphanException;
import model.exceptions.NonexistentEntityException;
import model.exceptions.RollbackFailureException;

/**
 *
 * @author panic
 */
public class StudentJpaController implements Serializable {

    public StudentJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Student student) throws RollbackFailureException, Exception {
        if (student.getStudentPreferenceCollection() == null) {
            student.setStudentPreferenceCollection(new ArrayList<StudentPreference>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<StudentPreference> attachedStudentPreferenceCollection = new ArrayList<StudentPreference>();
            for (StudentPreference studentPreferenceCollectionStudentPreferenceToAttach : student.getStudentPreferenceCollection()) {
                studentPreferenceCollectionStudentPreferenceToAttach = em.getReference(studentPreferenceCollectionStudentPreferenceToAttach.getClass(), studentPreferenceCollectionStudentPreferenceToAttach.getId());
                attachedStudentPreferenceCollection.add(studentPreferenceCollectionStudentPreferenceToAttach);
            }
            student.setStudentPreferenceCollection(attachedStudentPreferenceCollection);
            em.persist(student);
            for (StudentPreference studentPreferenceCollectionStudentPreference : student.getStudentPreferenceCollection()) {
                Student oldStudentIdOfStudentPreferenceCollectionStudentPreference = studentPreferenceCollectionStudentPreference.getStudentId();
                studentPreferenceCollectionStudentPreference.setStudentId(student);
                studentPreferenceCollectionStudentPreference = em.merge(studentPreferenceCollectionStudentPreference);
                if (oldStudentIdOfStudentPreferenceCollectionStudentPreference != null) {
                    oldStudentIdOfStudentPreferenceCollectionStudentPreference.getStudentPreferenceCollection().remove(studentPreferenceCollectionStudentPreference);
                    oldStudentIdOfStudentPreferenceCollectionStudentPreference = em.merge(oldStudentIdOfStudentPreferenceCollectionStudentPreference);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Student student) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Student persistentStudent = em.find(Student.class, student.getId());
            Collection<StudentPreference> studentPreferenceCollectionOld = persistentStudent.getStudentPreferenceCollection();
            Collection<StudentPreference> studentPreferenceCollectionNew = student.getStudentPreferenceCollection();
            List<String> illegalOrphanMessages = null;
            for (StudentPreference studentPreferenceCollectionOldStudentPreference : studentPreferenceCollectionOld) {
                if (!studentPreferenceCollectionNew.contains(studentPreferenceCollectionOldStudentPreference)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StudentPreference " + studentPreferenceCollectionOldStudentPreference + " since its studentId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<StudentPreference> attachedStudentPreferenceCollectionNew = new ArrayList<StudentPreference>();
            for (StudentPreference studentPreferenceCollectionNewStudentPreferenceToAttach : studentPreferenceCollectionNew) {
                studentPreferenceCollectionNewStudentPreferenceToAttach = em.getReference(studentPreferenceCollectionNewStudentPreferenceToAttach.getClass(), studentPreferenceCollectionNewStudentPreferenceToAttach.getId());
                attachedStudentPreferenceCollectionNew.add(studentPreferenceCollectionNewStudentPreferenceToAttach);
            }
            studentPreferenceCollectionNew = attachedStudentPreferenceCollectionNew;
            student.setStudentPreferenceCollection(studentPreferenceCollectionNew);
            student = em.merge(student);
            for (StudentPreference studentPreferenceCollectionNewStudentPreference : studentPreferenceCollectionNew) {
                if (!studentPreferenceCollectionOld.contains(studentPreferenceCollectionNewStudentPreference)) {
                    Student oldStudentIdOfStudentPreferenceCollectionNewStudentPreference = studentPreferenceCollectionNewStudentPreference.getStudentId();
                    studentPreferenceCollectionNewStudentPreference.setStudentId(student);
                    studentPreferenceCollectionNewStudentPreference = em.merge(studentPreferenceCollectionNewStudentPreference);
                    if (oldStudentIdOfStudentPreferenceCollectionNewStudentPreference != null && !oldStudentIdOfStudentPreferenceCollectionNewStudentPreference.equals(student)) {
                        oldStudentIdOfStudentPreferenceCollectionNewStudentPreference.getStudentPreferenceCollection().remove(studentPreferenceCollectionNewStudentPreference);
                        oldStudentIdOfStudentPreferenceCollectionNewStudentPreference = em.merge(oldStudentIdOfStudentPreferenceCollectionNewStudentPreference);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = student.getId();
                if (findStudent(id) == null) {
                    throw new NonexistentEntityException("The student with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Student student;
            try {
                student = em.getReference(Student.class, id);
                student.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The student with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StudentPreference> studentPreferenceCollectionOrphanCheck = student.getStudentPreferenceCollection();
            for (StudentPreference studentPreferenceCollectionOrphanCheckStudentPreference : studentPreferenceCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Student (" + student + ") cannot be destroyed since the StudentPreference " + studentPreferenceCollectionOrphanCheckStudentPreference + " in its studentPreferenceCollection field has a non-nullable studentId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(student);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Student> findStudentEntities() {
        return findStudentEntities(true, -1, -1);
    }

    public List<Student> findStudentEntities(int maxResults, int firstResult) {
        return findStudentEntities(false, maxResults, firstResult);
    }

    private List<Student> findStudentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Student.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Student findStudent(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Student.class, id);
        } finally {
            em.close();
        }
    }

    public int getStudentCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Student> rt = cq.from(Student.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
